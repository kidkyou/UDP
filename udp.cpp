#include "stdafx.h"
#include <winsock2.h>
#include <iostream>
#include <stdio.h>

using namespace std;

#pragma comment(lib, "Ws2_32.lib")

void InitWinsock()
{
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
}

int main()
{
	SOCKET socketS;
	int DEFAULT_PORT = 8080;

	InitWinsock();
	struct sockaddr_in local;

	local.sin_family = AF_INET;
	local.sin_port = htons(DEFAULT_PORT);
	local.sin_addr.s_addr = INADDR_ANY;

	struct sockaddr_in from;
	int fromlen = sizeof(from);
	
	socketS = socket(AF_INET, SOCK_DGRAM, 0);
	bind(socketS, (SOCKADDR *)&local, sizeof(local));
	cout << "Server Up..." << endl;

	char buffer[512];

	cout << "\ngoing to receive message...\n";
	do {
		int rc = recvfrom(socketS, buffer, sizeof(buffer), 0, (struct sockaddr *)&from, &fromlen);
		if (rc>0)
		{
			cout << "ERROR READING FROM SOCKET";
		}
		cout << "\n the message received is : " << buffer << endl;

		int rp = sendto(socketS, "", 2, 0, (struct sockaddr *)&from, fromlen);

		if (rp<0)
		{
			cout << "ERROR writing to SOCKET";
		}
	} while (1);

	/*do {
		int results = recvfrom(socketS, buffer, 512, 0, (SOCKADDR *)&from, &fromlen);
		ZeroMemory(buffer, 512);
		if (results < 0) {
			cout << "Error Reading From Socket" << endl;
		}
		else {
			cout << "Message received :" << buffer << endl;
			
		}
		int rp = sendto(sockectS, "hi", 2, 0, (SOCKADDR *)&from, fromlen);
		/*if (results != SOCKET_ERROR) {
			cout << "Client Disconnect!" << endl;
			break;
		}
		else{
			cout << "Data Diterima :" << buffer << endl;
			sendto(socketS, buffer, 512, 0, (SOCKADDR *)&from, fromlen);
		}
		Sleep(500);
	} while (1);
	*/
	/*while (1)
	{
		char buffer[1024];
		ZeroMemory(buffer, sizeof(buffer));
		printf("Waiting...\n");
		if (recvfrom(socketS, buffer, sizeof(buffer), 0, (sockaddr*)&from, &fromlen) != SOCKET_ERROR)
		{
			printf("Received message from %s: %s\n", inet_ntoa(from.sin_addr), buffer);
			sendto(socketS, buffer, sizeof(buffer), 0, (sockaddr*)&from, fromlen);
		}
		Sleep(500);
	}
	*/
	closesocket(socketS);

	return 0;
}
