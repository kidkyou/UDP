#include "stdafx.h"
#include <winsock2.h>
#include <WS2tcpip.h>
#include <stdio.h>
#include <iostream>
using namespace std;

#pragma comment (lib, "Ws2_32.lib")

void InitWinsock()
{
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
}

int main()
{
	SOCKET socketC;

	InitWinsock();
	struct sockaddr_in serverInfo;
	int len = sizeof(serverInfo);
	serverInfo.sin_family = AF_INET;
	serverInfo.sin_port = htons(8080);
	InetPton(AF_INET, _T("127.0.0.1"), &serverInfo.sin_addr.s_addr);
	//serverInfo.sin_addr.s_addr = inet_addr("127.0.0.1");

	socketC = socket(AF_INET, SOCK_DGRAM, 0);

	cout << "\ngoing to send message...\n";

	do
	{
		char buffer[1024];
		ZeroMemory(buffer, sizeof(buffer));

		cout << "\nMasukkan pesan :\n";
		fgets(buffer, 256, stdin);
		sendto(socketC, buffer, sizeof(buffer), 0, (struct sockaddr *)&serverInfo, len);
		recvfrom(socketC, buffer, 256, 0, (struct sockaddr *)&serverInfo, &len);
		if (sendto(socketC, buffer, sizeof(buffer), 0, (sockaddr*)&serverInfo, len) != SOCKET_ERROR) {
			if (recvfrom(socketC, buffer, sizeof(buffer), 0, (sockaddr*)&serverInfo, &len) != SOCKET_ERROR)
			{
				printf("Respons dari server: %s\n", buffer);
			}
		}
		/*cout << "Masukkan pesan :" << endl;
		//printf("Please input your message: ");
		cin >> buffer;
		if (strcmp(buffer, "exit") == 0)
			break;
		if (sendto(socketC, buffer, sizeof(buffer), 0, (sockaddr*)&serverInfo, len) != SOCKET_ERROR)
		{
			if (recvfrom(socketC, buffer, sizeof(buffer), 0, (sockaddr*)&serverInfo, &len) != SOCKET_ERROR)
			{
				printf("Receive response from server: %s\n", buffer);
			}
		}*/

	} while (1);
	closesocket(socketC);

	return 0;
}